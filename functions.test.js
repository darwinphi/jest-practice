const functions = require('./functions')

// test('Add 2 + 2 to equal 4', () => {
// 	expect(functions.add(2, 2)).toBe(4)
// })

// test('Add 2 + 2 to NOT equal 4', () => {
// 	expect(functions.add(2, 2)).not.toBe(5)
// })

// // toBeNull

// test('Should be null', () => {
// 	expect(functions.isNull(2, 2)).toBeNull()
// })

// test('Should be falsy', () => {
// 	expect(functions.checkValue(null)).toBeFalsy()
// })

// test('User should be Darwin Manalo object', () => {
// 	expect(functions.createUser()).toEqual({ firstName: 'Darwin', lastName: 'Manalo'})
// })

// test('Should be under 100', () => {
// 	const a = 50, b = 30
// 	expect(a + b).toBeLessThan(100)
// })

// test('Should be under 100', () => {
// 	const a = 50, b = 30
// 	expect(a + b).toBeLessThanOrEqual(100)
// })

// test('There is no `x` in Darwin', () => {
// 	expect('Darwin').not.toMatch(/x/)
// })

// test('Admin should be in usernames', () => {
// 	usernames = ['darwin', 'john', 'admin']

// 	expect(usernames).toContain('admin')
// })

// Async data
test('User fetched should be Leanne Graham', () => {
	expect.assertions(1)
	return functions.fetchUser().then(data => {
		expect(data.name).toEqual('Leanne Graham')
	})
})

// Async await
test('User fetched should be Leanne Graham', async () => {
	expect.assertions(1)
	const data = await functions.fetchUser()
	expect(data.name).toEqual('Leanne Graham')

})